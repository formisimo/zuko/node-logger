# zuko-node-logger

Logger interface for Winston used by Zuko Node.js projects

## Installation

```
npm install gitlab:Zuko.io/zuko-node-logger
```

## Usage

After installing, use the logger like so in a project:

```ecmascript 6
// log_test.js

const zukoLogger = require('@zuko/logger');

const logger = zukoLogger.createInstance();

logger.info('This is an info message');
logger.debug('This is a debug message');
```

```bash
node log_test.js

info:  This is an info message
debug: This is a debug message
```

### Metadata

Metadata is combined in this order:

[Message](#message) > [Instance](#instance) > [Default](#default)


#### Message

Each logged message can have metadata:

```ecmascript 6
// log_test.js

const zukoLogger = require('@zuko/logger');

const logger = zukoLogger.createInstance();

logger.debug('This is a debug message', {someMetadata: 'foo'});
```

```bash
node log_test.js

debug: This is a debug message {"someMetadata":"foo"}
```

#### Instance

Each logger instance can have metadata that is included with each message on that instance:

```ecmascript 6
// log_test.js

const zukoLogger = require('@zuko/logger');

const logger = zukoLogger.createInstance({requestId: 'abc-123'});

logger.debug('This is a debug message', {someMetadata: 'foo'});
logger.debug('This is a debug message', {requestId: 'xyz-321', someMetadata: 'foo'});
```

```bash
node log_test.js

debug: This is a debug message {"requestId":"abc-123","someMetadata":"foo"}
debug: This is a debug message {"requestId":"xyz-321","someMetadata":"foo"}
```

#### Default

Default metadata can be set and is used for all instances of the logger:

```ecmascript 6
// log_test.js

const zukoLogger = require('@zuko/logger');

zukoLogger.DEFAULT_METADATA.containerId = '589ef';

const logger = zukoLogger.createInstance({requestId: 'abc-123'});

logger.debug('This is a debug message', {someMetadata: 'foo'});
logger.debug('This is a debug message', {someMetadata: 'foo', requestId: 'xyz-321', containerId: 'aaa'});
```

```bash
node log_test.js

debug: This is a debug message {"containerId":"589ef","requestId":"abc-123","someMetadata":"foo"}
debug: This is a debug message {"containerId":"aaa","requestId":"xyz-321","someMetadata":"foo"}
```

### Levels

The following levels are available:

* `critical`
* `error`
* `warn`
* `info`
* `verbose`
* `debug`
* `trace`

When the `NODE_ENV` environment variable is set to `production` or `prod` the logger will output messages with `info` level or lower. With any other `NODE_ENV`, the logger will output log messages with a `debug` level or lower.

The level of the logger can be overridden by providing the `LOG_LEVEL` environment variable with one of the above levels.

### Silence

Logging can be turned off by setting the `LOG_ENABLED` environment variable to `false`:

```bash
LOG_ENABLED=false
```

### `esLogger`

An [elasticsearch.js](https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/index.html) compatible logger is included in this repo and can be used like so:

```ecmascript 6
// es_logger_test.js

const es = require('elasticsearch');
const esLogger = require('@zuko/logger/es');

const esClient = new es.Client({
  log: esLogger.createConstructor({requestId: 'abc-123'}),
});

esClient.search();
```

```bash
node es_logger_test.js

#=> info:   Adding connection to http://localhost:9200/ {"requestId":"abc-123"}
#=> debug:  starting request { method: 'POST', path: '/_search', query: {} } {"requestId":"abc-123"}
#=> debug:  Request complete {"requestId":"abc-123"}
```

## Development

    npm install

## Tests

    npm test
